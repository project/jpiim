<?php

	function jpiim_url_clear_cache() {
		file_scan_directory(file_directory_path() . '/jpiim/cache', '.*', array(), 'file_delete', TRUE);

		drupal_set_message('<br />JPIIM Cache has been cleared.');

		return;
	}
