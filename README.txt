
JPIIM - Just Put It In Me
------------------------
Copyright 2009 Steven C Jackson
Licensed under the GNU GPLv2 http://www.gnu.org/licenses/gpl-2.0.html

The license is included with the distribution of this software

To install, place the entire JPIIM folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the JPIIM module.

Now go to Administer -> Site Building -> Jpiim settings. Enter a descriptive name in the Block Description field and a well-formed URL in the 'URL To Pull In' field.

You may clear the cache by using the "Clear JPIIM Cache" button.

Blocks are then available to moved to whichever location you require in the Administer -> Site building -> Blocks area and are easily identifiable by the 'JPIIM' prefix along with whatever you put in the description field.

Maintainers
-----------
JPIIM was originally developed by:
Steven C Jackson
Roger Soper


